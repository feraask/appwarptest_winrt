﻿using AppWarpTestWinRT.Common;
using com.shephertz.app42.gaming.multiplayer.client;
using com.shephertz.app42.gaming.multiplayer.client.command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
// The Pivot Application template is documented at http://go.microsoft.com/fwlink/?LinkID=391641

namespace AppWarpTestWinRT
{
	// This class is used as a message entry in the list so that the list automatically scrolls to the bottom when a new entry is added.
	//	If you just use string as the type of the ObservableCollection messageList then the list doesn't scroll as it simply finds the
	//	first occurance of the string and scrolls to that. So if you have the string "Connected Successfully" multiple times added to
	//	the list, it only scrolls to the first one. This class makes each of those entries unique so when the messages_CollectionChanged
	//	is called it scrolls to the right entry.
	public class MessageEntry
	{
		public int id { get; set; }
		public string message { get; set; }

		public MessageEntry(int id, string message)
		{
			this.id = id;
			this.message = message;
		}
	}
	public sealed partial class PivotPage : Page
	{
		private readonly NavigationHelper navigationHelper;
		public ObservableCollection<MessageEntry> messageList { get; set; }

		public PivotPage()
		{
			this.InitializeComponent();

			this.navigationHelper = new NavigationHelper(this);
			this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
			this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
			this.messageList = new ObservableCollection<MessageEntry>();
			this.messageList.CollectionChanged += this.messages_CollectionChanged;
			AppServices.InitAppWarp(this);
			this.DataContext = this;
		}

		/// <summary>
		/// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
		/// </summary>
		public NavigationHelper NavigationHelper
		{
			get { return this.navigationHelper; }
		}

		/// <summary>
		/// Populates the page with content passed during navigation. Any saved state is also
		/// provided when recreating a page from a prior session.
		/// </summary>
		/// <param name="sender">
		/// The source of the event; typically <see cref="NavigationHelper"/>.
		/// </param>
		/// <param name="e">Event data that provides both the navigation parameter passed to
		/// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
		/// a dictionary of state preserved by this page during an earlier
		/// session. The state will be null the first time a page is visited.</param>
		private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
		{
		}

		/// <summary>
		/// Preserves state associated with this page in case the application is suspended or the
		/// page is discarded from the navigation cache. Values must conform to the serialization
		/// requirements of <see cref="SuspensionManager.SessionState"/>.
		/// </summary>
		/// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/>.</param>
		/// <param name="e">Event data that provides an empty dictionary to be populated with
		/// serializable state.</param>
		private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
		{
			// TODO: Save the unique state of the page here.
		}

		#region NavigationHelper registration

		/// <summary>
		/// The methods provided in this section are simply used to allow
		/// NavigationHelper to respond to the page's navigation methods.
		/// <para>
		/// Page specific logic should be placed in event handlers for the  
		/// <see cref="NavigationHelper.LoadState"/>
		/// and <see cref="NavigationHelper.SaveState"/>.
		/// The navigation parameter is available in the LoadState method 
		/// in addition to page state preserved during an earlier session.
		/// </para>
		/// </summary>
		/// <param name="e">Provides data for navigation methods and event
		/// handlers that cannot cancel the navigation request.</param>
		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			this.navigationHelper.OnNavigatedTo(e);
		}

		protected override void OnNavigatedFrom(NavigationEventArgs e)
		{
			this.navigationHelper.OnNavigatedFrom(e);
		}

		#endregion

		private void connectButton_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.inputBox.Text))
				this.inputBox.Text = "player";
			this._printMessage("Connecting with username: " + this.inputBox.Text);
			WarpClient.GetInstance().Connect(this.inputBox.Text);
			this.inputBox.Text = "";
		}

		private async void joinRoomButton_Click(object sender, RoutedEventArgs e)
		{
			// Create a room if no roomID is provided
			if (string.IsNullOrWhiteSpace(this.inputBox.Text))
			{
				await this.PrintMessage("creating a new room...");
				WarpClient.GetInstance().CreateRoom("room", "user", 5, null);
			}
			else
			{
				await this.PrintMessage("joining & subscribing to room: " + this.inputBox.Text);
				WarpClient.GetInstance().JoinRoom(this.inputBox.Text);
				WarpClient.GetInstance().SubscribeRoom(this.inputBox.Text);
			}
			this.inputBox.Text = "";
		}

		private void disconnectButton_Click(object sender, RoutedEventArgs e)
		{
			this._printMessage("Disconnecting...");
			WarpClient.GetInstance().Disconnect();
		}

		private async void clearButton_Click(object sender, RoutedEventArgs e)
		{
			await this.ClearMessages();
		}

		public async Task PrintMessage(string message)
		{
			if (Dispatcher.HasThreadAccess)
			{
				this._printMessage(message);
			}
			else
			{
				await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
				{
					this._printMessage(message);
				});
			}
		}

		private void _printMessage(string message)
		{
			lock (this.messageList)
			{
				this.messageList.Add(new MessageEntry(this.messageList.Count, message));
				Debug.WriteLine(message);
			}
		}

		public async Task ClearMessages()
		{
			if (Dispatcher.HasThreadAccess)
			{
				this.messageList.Clear();
			}
			else
			{
				await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
				{
					this.messageList.Clear();
				});
			}
		}

		private void messages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			var selectedIndex = this.messageListView.Items.Count - 1;
			if (selectedIndex < 0)
				return;

			this.messageListView.SelectedIndex = selectedIndex;
			this.messageListView.UpdateLayout();

			this.messageListView.ScrollIntoView(this.messageListView.SelectedItem);
		}
	}


}
