﻿using com.shephertz.app42.gaming.multiplayer.client;
using com.shephertz.app42.gaming.multiplayer.client.command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppWarpTestWinRT
{
	public class AppServices
	{
		public static string apiKey = "REPLACE_WITH_API_KEY";
		public static string secretKey = "REPLACE_WITH_SECRET_KEY";

		public static void InitAppWarp(PivotPage page)
		{
			WarpClient.initialize(AppServices.apiKey, AppServices.secretKey);
			WarpClient.setRecoveryAllowance(30);
			WarpClient.GetInstance().AddConnectionRequestListener(new ConnectionListener(page));
			WarpClient.GetInstance().AddNotificationListener(new NotificationListener(page));
			WarpClient.GetInstance().AddUpdateRequestListener(new UpdateListener(page));
			WarpClient.GetInstance().AddRoomRequestListener(new RoomListener(page));
			WarpClient.GetInstance().AddZoneRequestListener(new ZoneListener(page));
		}
	}


	public class ConnectionListener : com.shephertz.app42.gaming.multiplayer.client.listener.ConnectionRequestListener
	{
		public PivotPage _page = null;

		public ConnectionListener()
		{ }

		public ConnectionListener(PivotPage page)
		{ this._page = page; }

		public void onConnectDone(com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent eventObj)
		{
			byte result = eventObj.getResult();
			string msg = null;

			switch (result)
			{
				case WarpResponseResultCode.SUCCESS:
					msg = "Connected Successfully!";
					break;
				case WarpResponseResultCode.AUTH_ERROR:
					byte reasonCode = eventObj.getReasonCode();
					switch (reasonCode)
					{
						case WarpReasonCode.WAITING_FOR_PAUSED_USER:
							msg = "Connect AUTH_ERROR Reason Code: Waiting for Paused User (" + reasonCode + ")";
							break;
						default:
							msg = "Connect AUTH_ERROR Reason Code: " + reasonCode;
							break;
					}
					break;
				case WarpResponseResultCode.SUCCESS_RECOVERED:
					msg = "Connection Recovered!";
					break;
				case WarpResponseResultCode.CONNECTION_ERROR_RECOVERABLE:
					_page.PrintMessage("Connection error recoverable occured. waiting 15 seconds to recover...").Wait();
					Task.Delay(15000).Wait();
					_page.PrintMessage("recovering...").Wait();
					WarpClient.GetInstance().RecoverConnection();
					return;
				default:
					msg = "unknown connection error occured";
					break;
			}

			_page.PrintMessage(msg).Wait();
		}

		public void onDisconnectDone(com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent eventObj)
		{
			byte result = eventObj.getResult();
			string msg = null;

			switch (result)
			{
				case WarpResponseResultCode.SUCCESS:
					msg = "Disconnect Successfully!";
					break;
				case WarpResponseResultCode.AUTH_ERROR:
					byte reasonCode = eventObj.getReasonCode();
					switch (reasonCode)
					{
						case WarpReasonCode.WAITING_FOR_PAUSED_USER:
							msg = "Disconnect AUTH_ERROR Reason Code: Waiting for Paused User (" + reasonCode + ")";
							break;
						default:
							msg = "Disconnect AUTH_ERROR Reason Code: " + reasonCode;
							break;
					}
					break;
				default:
					msg = "unknown disconnect error occured";
					break;
			}

			_page.PrintMessage(msg).Wait();
		}

		public void onInitUDPDone(byte resultCode)
		{

		}
	}

	public class NotificationListener : com.shephertz.app42.gaming.multiplayer.client.listener.NotifyListener
	{
		private PivotPage _page;
		public NotificationListener()
		{ }

		public NotificationListener(PivotPage currPage)
		{ _page = currPage; }

		public void onChatReceived(com.shephertz.app42.gaming.multiplayer.client.events.ChatEvent eventObj)
		{
		}

		public void onGameStarted(string sender, string roomId, string nextTurn)
		{
		}

		public void onGameStopped(string sender, string roomId)
		{
		}

		public void onMoveCompleted(com.shephertz.app42.gaming.multiplayer.client.events.MoveEvent moveEvent)
		{
		}

		public void onPrivateChatReceived(string sender, string message)
		{
		}

		public void onPrivateUpdateReceived(string sender, byte[] update, bool fromUdp)
		{
		}

		public void onRoomCreated(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj)
		{
		}

		public void onRoomDestroyed(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj)
		{
		}

		public void onUpdatePeersReceived(com.shephertz.app42.gaming.multiplayer.client.events.UpdateEvent eventObj)
		{
		}

		public void onUserChangeRoomProperty(com.shephertz.app42.gaming.multiplayer.client.events.RoomData roomData, string sender, Dictionary<string, object> properties, Dictionary<string, string> lockedPropertiesTable)
		{
		}

		public void onUserJoinedLobby(com.shephertz.app42.gaming.multiplayer.client.events.LobbyData eventObj, string username)
		{
		}

		public void onUserLeftLobby(com.shephertz.app42.gaming.multiplayer.client.events.LobbyData eventObj, string username)
		{
		}

		public void onUserJoinedRoom(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj, string username)
		{
			_page.PrintMessage(username + " joined room").Wait();
		}

		public void onUserLeftRoom(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj, string username)
		{
			_page.PrintMessage(username + " left room").Wait();
		}

		public void onUserPaused(string locid, bool isLobby, string username)
		{
			_page.PrintMessage(username + " paused").Wait();
		}

		public void onUserResumed(string locid, bool isLobby, string username)
		{
			_page.PrintMessage(username + " resumed. My connection state: " + WarpClient.GetInstance().GetConnectionState()).Wait();
		}

		public void onNextTurnRequest(string lastTurn)
		{
		}
	}

	public class UpdateListener : com.shephertz.app42.gaming.multiplayer.client.listener.UpdateRequestListener
	{
		public PivotPage _page;

		public UpdateListener()
		{
		}

		public UpdateListener(PivotPage currPage)
		{
			_page = currPage;
		}

		public void onSendPrivateUpdateDone(byte result)
		{

		}

		public void onSendUpdateDone(byte result)
		{

		}
	}

	public class RoomListener : com.shephertz.app42.gaming.multiplayer.client.listener.RoomRequestListener
	{
		private PivotPage _page;
		public RoomListener(PivotPage page)
		{ this._page = page; }

		public void onGetLiveRoomInfoDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent eventObj)
		{
		}

		public void onJoinRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			if (eventObj.getResult() == WarpResponseResultCode.SUCCESS)
			{
				string roomID = eventObj.getData().getId();
				_page.PrintMessage("joined room: " + roomID).Wait();
			}
			else
			{
				_page.PrintMessage("join room error code: " + eventObj.getResult()).Wait();
			}
		}

		public void onLeaveRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onLockPropertiesDone(byte result)
		{
		}

		public void onSetCustomRoomDataDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent eventObj)
		{
		}

		public void onSubscribeRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			if (eventObj.getResult() == WarpResponseResultCode.SUCCESS)
			{
				string roomID = eventObj.getData().getId();
				_page.PrintMessage("subscribed to room: " + roomID).Wait();
			}
			else
			{
				_page.PrintMessage("subscribe room error code: " + eventObj.getResult()).Wait();
			}
		}

		public void onUnSubscribeRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onUnlockPropertiesDone(byte result)
		{
		}

		public void onUpdatePropertyDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent lifeLiveRoomInfoEvent)
		{
		}
	}

	public class ZoneListener : com.shephertz.app42.gaming.multiplayer.client.listener.ZoneRequestListener
	{
		private PivotPage _page;

		public ZoneListener(PivotPage page)
		{ this._page = page; }

		public void onCreateRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			if (eventObj.getResult() == WarpResponseResultCode.SUCCESS)
			{
				string roomID = eventObj.getData().getId();
				_page.PrintMessage("created room id: " + roomID).Wait();
				_page.PrintMessage("joining & subscribing to room...").Wait();
				WarpClient.GetInstance().JoinRoom(roomID);
				WarpClient.GetInstance().SubscribeRoom(roomID);
			}
			else
			{
				_page.PrintMessage("error creating room, error code: " + eventObj.getResult()).Wait();
			}
		}

		public void onDeleteRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onGetAllRoomsDone(com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent eventObj)
		{
		}

		public void onGetLiveUserInfoDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent eventObj)
		{
		}

		public void onGetMatchedRoomsDone(com.shephertz.app42.gaming.multiplayer.client.events.MatchedRoomsEvent matchedRoomsEvent)
		{
		}

		public void onGetOnlineUsersDone(com.shephertz.app42.gaming.multiplayer.client.events.AllUsersEvent eventObj)
		{
		}

		public void onSetCustomUserDataDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent eventObj)
		{
		}
	}

	
}
